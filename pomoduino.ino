#include "TM1637Display.h"

#define CLK         9
#define DIO         8

#define MIN_INDEX   4
#define SEC_INDEX   2

#define ONE_SECOND  1000

#define POMODORO_DURATION 25 * 60
#define SHORT_BREAK_DURATION 5 * 60
#define LONG_BREAK_DURATION 15 * 60

TM1637Display display(CLK, DIO);

int session = 0;
int count = 0;
unsigned long remaining_time = 0;

void setup()
{
  Serial.begin(9600);
}

void loop()
{
  reset_display();
  switch (session)
  {
    case 0 :
      run_pomodoro();
      break;
    case 1 :
      run_short_break();
      break;
    case 2 :
      run_long_break();
      break;
    default:
      while(1);
      break;
  }
}

void reset_display (void)
{
  display.setBrightness(2);
  uint8_t data[] = { 0x00, 0x00, 0x00, 0x00 };
  display.setSegments(data);
}

void run_pomodoro(void)
{
  remaining_time = POMODORO_DURATION;
  roll_session ();
  branch_to_break (++count);
}

void run_short_break (void)
{
  remaining_time = SHORT_BREAK_DURATION;
  roll_session ();
  branch_to_pomodoro();
}

void run_long_break (void)
{
  remaining_time = LONG_BREAK_DURATION;
  roll_session ();
  branch_to_pomodoro();
}

void roll_session ()
{
  while (remaining_time > 0)
  {
    update_display(remaining_time--);
    delay(ONE_SECOND);
  } 
}

void update_display (unsigned long session_time)
{
  display.showNumberDec( (session_time % 60), true, 2, SEC_INDEX ); 
  display.showNumberDecEx( round(session_time / 60), 0xff, false, 2, MIN_INDEX ); 
}

void branch_to_break (int session_counter)
{
  if ((session_counter % 4) == 0)
  {
    session = 2;
  }
  else
  {
    session = 1;
  }
}

void branch_to_pomodoro (void)
{
  session = 0;
}

